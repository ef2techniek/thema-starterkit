import { createApp } from "vue";

window.axios = require('axios')
window.lodash = require('lodash')

import YoutubeWrapper from './components/YoutubeWrapper'

const app = createApp({})

app.component('ef2-youtube-wrapper', YoutubeWrapper)

app.mount('#page-wrapper')