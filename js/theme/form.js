// Make description of checkbox clickable
let checkboxFromItems = document.querySelectorAll('.form-type-checkbox');

if (!_.isEmpty(checkboxFromItems)) {
    checkboxFromItems.forEach((checkbox) => {
        let checkboxDescription = checkbox.querySelector('.description');

        if (!_.isNil(checkboxDescription)) {
            checkboxDescription.style.cursor = 'pointer';

            checkboxDescription.addEventListener('click', (e) => {

                if (e.target.tagName === 'A') {
                    return;
                }

                let checkboxInput = checkbox.querySelector('input');

                if (_.isNil(checkboxInput)) {
                    return;
                }

                checkboxInput.checked = !checkboxInput.checked;
            })
        }
    })
}