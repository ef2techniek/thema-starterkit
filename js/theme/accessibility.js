// Give all links with target blank a span with accessible text
const targetBlankLinks = document.querySelectorAll('a[target="_blank"]');
const opensInNewWindowText = window.Drupal.t('(Opens in a new window)')

if (!_.isEmpty(targetBlankLinks)) {
    targetBlankLinks.forEach(link => {
        const targetBlankTag = document.createElement('span');
        const targetBlankText = document.createTextNode(opensInNewWindowText);

        targetBlankTag.appendChild(targetBlankText);
        targetBlankTag.classList.add('visually-hidden');

        link.appendChild(targetBlankTag);
    });
}

